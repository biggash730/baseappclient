'use strict';
var blumaApp = angular
    .module('blumaApp', ['ngRoute', 'ngCookies', 'ngResource', 'angularMoment', 'formly', 'ui.select']);

blumaApp.run(function($rootScope, $location, $cookies, $http) {
    $rootScope.$on("$routeChangeStart", function(event, next) {

        $rootScope.currentUser = $cookies.getObject('currentUser');
        if ($rootScope.currentUser == null) {
            // no logged user, redirect to /login
            if (next.templateUrl === "views/login.html") {
                $location.path("/login");
            } else if (next.templateUrl === "views/signup.html") {
                $location.path("/signup");
            } else if (next.templateUrl === "views/resetpassword.html") {
                $location.path("/resetpassword");
            } else {
                $location.path("/login");
            }
        } else {
            $rootScope.user = $cookies.getObject('currentUser');
            if (next.templateUrl === "views/login.html") {
                $location.path("/");
            } else if (next.templateUrl === "views/signup.html") {
                $location.path("/");
            } else if (next.templateUrl === "views/requestreset.html") {
                $location.path("/");
            } else if (next.templateUrl === "views/resetpassword.html") {
                $location.path("/");
            }            
        }
    });    

    $rootScope.baseUrl = 'https://bluma.azurewebsites.net/';
    //$rootScope.baseUrl = 'http://localhost:60356/';

    $rootScope.pageSize = 6;

    $http.defaults.headers.common['Authorization'] = "Bearer " + $cookies.get('token');
});

// configure our routes
blumaApp.config(function($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl',
        controllerAs: 'dashboard'
    })

    .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
        })
        .when('/signup', {
            templateUrl: 'views/signup.html',
            controller: 'SignupCtrl',
            controllerAs: 'signup'
        })
        .when('/resetpassword', {
            templateUrl: 'views/resetpassword.html',
            controller: 'ResetpasswordCtrl',
            controllerAs: 'resetpassword'
        })
        
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller: 'ProfileCtrl',
            controllerAs: 'profile'
        })
        
        .when('/reports', {
            templateUrl: 'views/reports.html',
            controller: 'ReportsCtrl',
            controllerAs: 'reports'
        })        
        .when('/newpage', {
          templateUrl: 'views/newpage.html',
          controller: 'NewpageCtrl',
          controllerAs: 'newpage'
        })
        .when('/anotherpage', {
          templateUrl: 'views/anotherpage.html',
          controller: 'AnotherpageCtrl',
          controllerAs: 'anotherpage'
        })
        .when('/sendmessage', {
          templateUrl: 'views/sendmessage.html',
          controller: 'SendmessageCtrl',
          controllerAs: 'sendmessage'
        })
        .when('/contacts', {
          templateUrl: 'views/contacts.html',
          controller: 'ContactsCtrl',
          controllerAs: 'contacts'
        })
        .when('/manageusers', {
          templateUrl: 'views/manageusers.html',
          controller: 'ManageusersCtrl',
          controllerAs: 'manageusers'
        })
        .when('/groups', {
          templateUrl: 'views/groups.html',
          controller: 'GroupsCtrl',
          controllerAs: 'groups'
        })
        .when('/senderids', {
          templateUrl: 'views/senderids.html',
          controller: 'SenderidsCtrl',
          controllerAs: 'senderids'
        })
        .when('/messagetemplates', {
          templateUrl: 'views/messagetemplates.html',
          controller: 'MessagetemplatesCtrl',
          controllerAs: 'messagetemplates'
        })
        .when('/uploadtemplate', {
          templateUrl: 'views/uploadtemplate.html',
          controller: 'UploadtemplateCtrl',
          controllerAs: 'uploadtemplate'
        })
        .when('/messages', {
          templateUrl: 'views/messages.html',
          controller: 'MessagesCtrl',
          controllerAs: 'messages'
        })
        .when('/topup', {
          templateUrl: 'views/topup.html',
          controller: 'TopupCtrl',
          controllerAs: 'topup'
        })
        .when('/quicksend', {
          templateUrl: 'views/quicksend.html',
          controller: 'QuicksendCtrl',
          controllerAs: 'quicksend'
        })
        .when('/schedulemessage', {
          templateUrl: 'views/schedulemessage.html',
          controller: 'SchedulemessageCtrl',
          controllerAs: 'schedulemessage'
        })
        .otherwise({
            redirectTo: "/"
        });
});

blumaApp.config(function(formlyConfigProvider) {
    formlyConfigProvider.setType({
        name: 'input',
        template: '<div class="field"><label>{{options.label}}</label><input type="text" ng-model="model[options.key]"></div>'
    });
});
