'use strict';
/**
 * @ngdoc function
 * @name blumaApp.controller:TopupCtrl
 * @description
 * # TopupCtrl
 * Controller of the blumaApp
 */
angular.module('blumaApp').controller('TopupCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Top Up";
    var vm = $scope;
    vm.makepayment = function(value) {
        NProgress.start();
        //console.log(value);
        setTimeout(function() {
            NProgress.done();
            switch (value) {
                case 5:
                    window.open('https://app.mpowerpayments.com/click2pay-redirect/59c2d2f5784e7559a7b75209');
                    break;
                case 10:
                window.open('https://app.mpowerpayments.com/click2pay-redirect/600ba37c8894de6a45330f3b');
                    break;
                case 20:
                window.open('https://app.mpowerpayments.com/click2pay-redirect/1408458f2498f04e90067311');
                    break;
                case 50:
                window.open('https://app.mpowerpayments.com/click2pay-redirect/6487cebb873ba3e42b3fdc0a');
                    break;
                case 100:
                window.open('https://app.mpowerpayments.com/click2pay-redirect/4ad03520ae64e12fa1c120f6');
                    break;
                case 200:
                window.open('https://app.mpowerpayments.com/click2pay-redirect/7d1845fb29faf6c3adc925d9');
                    break;
            }
        }, 3000);
    };
});
/*
5 - https://app.mpowerpayments.com/click2pay-redirect/59c2d2f5784e7559a7b75209
10 - 
20 - 
50 - 
100 - 
200 - 


  */