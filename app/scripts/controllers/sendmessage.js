'use strict';
angular.module('blumaApp').controller('SendmessageCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Send Messages";
    var vm = $scope;
    vm.message = {};
    vm.message.Contacts = [];
    vm.types = ["Email", "SMS"];
    vm.showtitle = false;
    vm.showSubject = function() {
        if (vm.message.MessageType == "Email") vm.showtitle = true;
        else vm.showtitle = false;
    };
    vm.templateSelected = function() {
        console.log(vm.message.Template);
        vm.message.Subject = vm.message.Template.Title;
        vm.message.Message = vm.message.Template.Message;
    };
    vm.getModels = function() {
        var obj = {
            Page: 0,
            Size: 0
        };
        $http.post($rootScope.baseUrl + 'settings/getgroups', obj).then(function(res) {
            if (res.data.success) {
                vm.message.Groups = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
        $http.post($rootScope.baseUrl + 'settings/gettemplates', obj).then(function(res) {
            if (res.data.success) {
                vm.templates = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
        $http.post($rootScope.baseUrl + 'settings/getsenders', obj).then(function(res) {
            if (res.data.success) {
                vm.senderids = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
    };
    vm.getModels()
    vm.showReceipients = function() {
        NProgress.start();
        var groups = [];
        vm.message.Groups.forEach(function(x) {
            if (x.Checked) groups.push(x);
        });
        $http.post($rootScope.baseUrl + 'getgroupcontacts', groups).then(function(res) {
            console.log(res.data);
            NProgress.done();
            if (res.data.success) {
                vm.message.Contacts = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
    };
    vm.remove = function(ind) {
        vm.message.Contacts.splice(ind, 1);
    }
    vm.sendMessage = function() {
        //validate the form
        if (vm.message.Contacts.length <= 0) toastr.error("There are no receipients.")
        else {
            NProgress.start();
            $http.post($rootScope.baseUrl + 'message/sendmessage', vm.message).then(function(res) {
                NProgress.done();
                //console.log(res.data);
                if (res.data.success) {                    
                    toastr.success(res.data.message);
                    $location.path("/messages");
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.dir(res);
                toastr.error('Reset Failed');
                NProgress.done();
            });
        }
    }
    /*vm.getContacts = function() {
        console.log(vm.message.Groups);
    };
    vm.page = 1;
    vm.totalRecords = 0;
    vm.add = function() {
        NProgress.start();
        $http.post($rootScope.baseUrl + 'settings/SaveGroup', vm.single).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                vm.clearModels();
                vm.getModel();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.editRec = function(itm) {
        vm.single = itm;
        vm.modalHeader = "Edit Group";
        $('#addGroupModal').modal('show');
    };
    vm.clearModels = function() {
        vm.single = {};
    };
    vm.clearLists = function() {
        vm.dataList = [];
    };
    vm.delRec = function(itm) {
        NProgress.start();
        swal({
            title: "Are you sure?",
            text: "Record will be deleted",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function() {
            $http.post($rootScope.baseUrl + 'Settings/DeleteGroup', itm).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success(res.data.message);
                    vm.clearLists();
                    vm.getModel();
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.log(res);
                toastr.error('Deletion Failed');
                NProgress.done();
            });
        });
        NProgress.done();
    };*/
});