'use strict';

/**
 * @ngdoc function
 * @name blumaApp.controller:MessagesCtrl
 * @description
 * # MessagesCtrl
 * Controller of the blumaApp
 */
angular.module('blumaApp')
  .controller('MessagesCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Message Logs";
    var vm = $scope;
    vm.type = "sent";
    vm.page = 1;
    vm.totalRecords = 0;
    $('.ui.dropdown').dropdown();
    vm.getModel = function() {
        NProgress.start();
        var obj = {
            Page: vm.page,
            Size: vm.pageSize,
            Status: vm.type
        };
        $http.post($rootScope.baseUrl + 'getmessagelogs',obj).then(function(res) {
            NProgress.done();
            if (res.data.success) {
                vm.dataList = res.data.data;
                vm.totalRecords = res.data.total;
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.getPending = function(){
    	vm.type = "pending";
    	vm.getModel();
    };
    vm.getSent = function(){
    	vm.type = "sent";
    	vm.getModel();
    };
    vm.nextPage = function() {
        if ((vm.pageSize * vm.page) < vm.totalRecords) {
            vm.page = vm.page + 1;
            vm.getModel();
        }
    };
    vm.previousPage = function() {
        if (vm.page > 1) {
            vm.page = vm.page - 1;
            vm.getModel();
        }
    };
    vm.getModel();

    vm.resend = function(itm){
        NProgress.start();
        $http.post($rootScope.baseUrl + 'message/resendmessage', itm).then(function(res) {
            NProgress.done();
            if (res.data.success) {
                toastr.success(res.data.message);
                vm.getModel();
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
});