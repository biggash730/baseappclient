'use strict';
/**
 * @ngdoc function
 * @name blumaApp.controller:UploadtemplateCtrl
 * @description
 * # UploadtemplateCtrl
 * Controller of the blumaApp
 */
angular.module('blumaApp').controller('UploadtemplateCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Login";
    var vm = $scope;
    vm.dataList = [];
    var groups = [];
    vm.goBack = function() {
        $location.path('/contacts');
    }
    vm.getGroups = function() {
        vm.groups = [];
        $http.post($rootScope.baseUrl + 'settings/Getgroups', {}).then(function(res) {
            if (res.data.success) {
                vm.groups = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
    };
    vm.getGroups();
    vm.save = function() {
        NProgress.start();
        vm.dataList.forEach(function(i) {
        	//i.DateOfBirth = moment(i.DateOfBirth,"DD/MM/YYYY").format('MM-DD-YYYY');
        	i.GroupIds = [];
            vm.groups.forEach(function(x) {
                if (x.Checked) i.GroupIds.push(x.Id);
            });
        });
        console.log(vm.dataList);
        $http.post($rootScope.baseUrl + 'contact/saveupload', vm.dataList).then(function(res) {
            NProgress.done();
            if (res.data.success) {
                toastr.success(res.data.message);
                $location.path('/contacts');
            }
        }, function(res) {
            NProgress.done();
            toastr.error(res);
        });
    }

    function browserSupportFileUpload() {
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            isCompatible = true;
        }
        return isCompatible;
    }
    $(":file").change(function() {
        NProgress.start();
        toastr.info("Upload started");
        if (!browserSupportFileUpload()) {
            toastr.error('The File APIs are not fully supported in this browser!');
            NProgress.done();
            return;
        } else {
            var file = this.files[0];
            Papa.parse(file, {
                header: true,
                worker: true,
                step: function(results) {
                    var obj = results.data[0];
                    var gp = vm.groups;
                    var nd = {
                        'Name': obj.Name,
                        'PhoneNumber': obj.PhoneNumber,
                        'Email': obj.Email/*,
                        'DateOfBirth': obj.DateOfBirth*/
                    };
                    if (nd.Name != "") vm.dataList.push(nd);
                },
                complete: function() {
                    NProgress.done();
                    toastr.success("All done!");
                    vm.$apply();
                }
            });
        }
    });
});