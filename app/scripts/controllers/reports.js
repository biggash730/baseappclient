'use strict';
angular.module('blumaApp').controller('ReportsCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Reports";
    var vm = $scope;
    vm.report = {};
    vm.showResults = false;
    vm.months = [{
        Name: "January",
        "Id": 1
    }, {
        Name: "February",
        "Id": 2
    }, {
        Name: "March",
        "Id": 3
    }, {
        Name: "April",
        "Id": 4
    }, {
        Name: "May",
        "Id": 5
    }, {
        Name: "June",
        "Id": 6
    }, {
        Name: "July",
        "Id": 7
    }, {
        Name: "August",
        "Id": 8
    }, {
        Name: "September",
        "Id": 9
    }, {
        Name: "October",
        "Id": 10
    }, {
        Name: "November",
        "Id": 11
    }, {
        Name: "December",
        "Id": 12
    }];
    vm.Generate = function() {
        //check if the year is valid
        if (!vm.report.Month) toastr.info("please select a month", 'Reports');
        else if (vm.report.Year && vm.report.Year < 1910) toastr.info("please enter a valid year", 'Reports');
        else {
            vm.ActiveMonth = vm.report.Month;
            vm.ActiveYear = vm.report.Year;
            NProgress.start();
            $http.post($rootScope.baseUrl + 'transactions/getreportData?month=' + vm.report.Month + '&year=' + vm.report.Year).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    vm.data = res.data.data.Data;
                    vm.TotalOutputVat = res.data.data.TotalOutputVat;
                    vm.TotalInputVat = res.data.data.TotalInputVat;
                    vm.showResults = true;
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                NProgress.done();
            });
        }
    };

    vm.Download = function () {
            NProgress.start();
            toastr.info("Final Report will be downloaded", 'Reports');
            $http.post($rootScope.baseUrl + 'transactions/downloadreport?month=' + vm.ActiveMonth + '&year=' + vm.ActiveYear).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    var file = "//" + res.data.data;
                    console.log(file);
                    window.open(file);
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                NProgress.done();
            });
        };
});