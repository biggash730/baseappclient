'use strict';

angular.module('blumaApp')
    .controller('LoginCtrl', function($scope, $cookies, $location, $rootScope, $http) {
        $rootScope.page = "Login";
        var vm = $scope;

        vm.user = {
            RememberMe: true
        };
        Waves.attach('.button', ['waves-button', 'waves-float', 'waves-light']);
        Waves.init();
        /*$(document).ready(function() {
            $('input#input_text').characterCounter();
        });*/

        vm.doLogin = function() {
            //todo: Validate the object here

            NProgress.start();
            $http.post($rootScope.baseUrl + 'Account/userlogin', vm.user).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success('Login Successful');
                    var usr = res.data.data;
                    var user = {};
                    user.fullname = usr.FullName;
                    user.phone = usr.PhoneNumber;
                    user.email = usr.Email;
                    user.username = usr.UserName;
                    user.dateofbirth = usr.DateOfBirth;
                    $cookies.putObject('currentUser', user);
                    $cookies.put('token', res.data.message);
                    $http.defaults.headers.common['Authorization'] = "Bearer " + res.data.message;
                    $location.path("/");
                } else {
                    toastr.error(res.data.message);
                }

            }, function(res) {
                console.dir(res);
                toastr.error('Login Failed');
                NProgress.done();
            });
        };
    });
