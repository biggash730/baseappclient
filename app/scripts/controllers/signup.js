'use strict';
angular.module('blumaApp').controller('SignupCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Minuti";
    var vm = $scope;
    vm.user = {};
    
    vm.register = function() {
        var err = '';
        if (!vm.user.FullName) err = err + 'Please enter a full name \n';
        if (!vm.user.UserName) err = err + 'Please enter a username \n';
        if (!vm.user.DateOfBirth) err = err + 'Please enter a Date of Birth \n';
        if (!vm.user.Email) err = err + 'Please enter your Email \n';
        if (err != '') {
            toastr.error(err);
            return;
        } else {
            NProgress.start();
            console.log(vm.user);
            //vm.user.DateOfBirth = moment(vm.user.DateOfBirth,"DD/MM/YYYY").format('MM-DD-YYYY');
            $http.post($rootScope.baseUrl + 'Account/signup', vm.user).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success('Successful, Please check your email for your password');
                    $location.path("/login");
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.log(res);
                toastr.error('Registration Failed');
                NProgress.done();
            });
        }
    };
});