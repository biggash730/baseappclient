'use strict';

angular.module('blumaApp')
  .controller('ContactsCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Contacts";
    var vm = $scope;
    vm.page = 1;
    vm.totalRecords = 0;
    $('.ui.dropdown').dropdown();
    vm.getModel = function() {
        NProgress.start();
        var rt = '';
        var obj = {
            Page: vm.page,
            Size: vm.pageSize
        };
        $http.post($rootScope.baseUrl + 'Contact/GetContacts', obj).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                vm.dataList = res.data.data;
                vm.totalRecords = res.data.total;
                //toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.addRec = function() {
        vm.clearModels();
        vm.modalHeader = "Add New Contact";
        $('#addContactModal').modal('show');
    };
    vm.add = function() {
    	NProgress.start();
        vm.single.GroupIds = [];
    	vm.groups.forEach(function(x){
    		if(x.Checked) vm.single.GroupIds.push(x.Id);
    	});
    	//console.log(vm.single);
    	//return;
    	$http.post($rootScope.baseUrl + 'Contact/SaveContactDetails', vm.single).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                vm.clearModels();
                vm.getModel();
                vm.getGroups();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.editRec = function(itm) {
        vm.groups.forEach(function(x){
            x.Checked = false
            itm.GroupIds.forEach(function(i){
                if(x.Id == i){
                    x.Checked = true;
                }
            });            
        });
        //itm.DateOfBirth = new Date(itm.DateOfBirth);
        vm.single = itm;
        vm.modalHeader = "Edit Contact Details";
        $('#addContactModal').modal('show');
    };
    vm.clearModels = function() {
        vm.single = {};
    };
    vm.clearLists = function() {
        vm.dataList = [];
    };
    vm.delRec = function(itm) {
        NProgress.start();
        swal({
            title: "Are you sure?",
            text: "Record will be deleted",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function() {
            $http.post($rootScope.baseUrl + 'Contact/DelContact', itm).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    //toastr.success(res.data.message);
                    vm.clearLists();
                    vm.getModel();
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.log(res);
                toastr.error('Deletion Failed');
                NProgress.done();
            });
        });
        NProgress.done();
    };
    vm.nextPage = function() {
    	console.log('next');
        if ((vm.pageSize * vm.page) < vm.totalRecords) {
            vm.page = vm.page + 1;
            vm.getModel();
        }
    };
    vm.previousPage = function() {
    	console.log('prev');
        if (vm.page > 1) {
            vm.page = vm.page - 1;
            vm.getModel();
        }
    };
    vm.getModel();
    vm.getGroups = function() {
    	vm.groups = [];
        $http.post($rootScope.baseUrl + 'settings/Getgroups', {}).then(function(res) {
            if (res.data.success) {
                vm.groups = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
    };
    vm.getGroups();

    vm.UploadFile = function() {
        $location.path('/uploadtemplate');
    };

    vm.DownloadTemplate = function() {
        var file = "https://dl.dropboxusercontent.com/u/90572675/Contacts_Template.csv";
            window.open(file);
    };

    vm.exportContacts = function(){
        setTimeout(function(){
            toastr.info('Export functionality coming soon');
        }, 1000)
    };
});