'use strict';
angular.module('blumaApp').controller('DashboardCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Dashboard";
    var vm = $scope;
    $('.tabular.menu .item').tab();

    vm.GetSummaries = function() {
        //NProgress.start();
        $http.get($rootScope.baseUrl + 'getsummaries').then(function(res) {
            //NProgress.done();
            if (res.data.success) {
                vm.summaries = res.data.data;
            } else {
                console.log(res.data.message);
            }
        }, function(res) {
            console.log(res);
            //NProgress.done();
        });
    };
    vm.GetSummaries();

    vm.GetMessages = function(status) {
        $http.get($rootScope.baseUrl + 'getmessages?status='+status).then(function(res) {
            if (res.data.success) {
                if(status == "pending"){
                    vm.pendingMessages = res.data.data;
                }
                else{
                    vm.sentMessages = res.data.data;
                }                
            } else {
                console.log(res.data.message);
            }
        }, function(res) {
            console.log(res);
            //NProgress.done();
        });
    };

    vm.GetMessages('pending');
    vm.GetMessages('sent');

    vm.GetContacts = function() {
        $http.get($rootScope.baseUrl + 'dashboard/contacts').then(function(res) {
            if (res.data.success) {
                vm.contacts = res.data.data;
            }
        }, function(res) {
            console.log(res);
        });
    };
    vm.GetContacts();
    vm.openMessages = function(type){
        console.log(type);
    };
});