'use strict';

/**
 * @ngdoc function
 * @name blumaApp.controller:SchedulemessageCtrl
 * @description
 * # SchedulemessageCtrl
 * Controller of the blumaApp
 */
angular.module('blumaApp')
  .controller('SchedulemessageCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
