'use strict';

angular.module('blumaApp')
  .controller('SenderidsCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Sender Ids";
    var vm = $scope;
    vm.page = 1;
    vm.totalRecords = 0;
    $('.ui.dropdown').dropdown();
    vm.getModel = function() {
        NProgress.start();
        var rt = '';
        var obj = {
            Page: vm.page,
            Size: vm.pageSize
        };
        $http.post($rootScope.baseUrl + 'settings/getsenders', obj).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                vm.dataList = res.data.data;
                vm.totalRecords = res.data.total;
                //toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.addRec = function() {
        vm.clearModels();
        vm.modalHeader = "Add New Sender";
        $('#addSenderModal').modal('show');
    };
    vm.add = function() {
    	NProgress.start();
    	$http.post($rootScope.baseUrl + 'settings/SaveSender', vm.single).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                vm.clearModels();
                vm.getModel();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.editRec = function(itm) {
        vm.single = itm;
        vm.modalHeader = "Edit Sender";
        $('#addSenderModal').modal('show');
    };
    vm.clearModels = function() {
        vm.single = {};
    };
    vm.clearLists = function() {
        vm.dataList = [];
    };
    vm.delRec = function(itm) {
        NProgress.start();
        swal({
            title: "Are you sure?",
            text: "Record will be deleted",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function() {
            $http.post($rootScope.baseUrl + 'Settings/DeleteSender', itm).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success(res.data.message);
                    vm.clearLists();
                    vm.getModel();
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.log(res);
                toastr.error('Deletion Failed');
                NProgress.done();
            });
        });
        NProgress.done();
    };
    vm.nextPage = function() {
        if ((vm.pageSize * vm.page) < vm.totalRecords) {
            vm.page = vm.page + 1;
            vm.getModel();
        }
    };
    vm.previousPage = function() {
        if (vm.page > 1) {
            vm.page = vm.page - 1;
            vm.getModel();
        }
    };
    vm.getModel();
});