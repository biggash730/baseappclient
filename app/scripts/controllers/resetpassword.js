'use strict';

angular.module('blumaApp')
  .controller('ResetpasswordCtrl', function($scope, $rootScope, $http, $location) {
        $rootScope.page = "Reset";
        var vm = $scope;        
        vm.user = {};

        vm.doReset = function() {
            //todo: Validate the object here
            NProgress.start();
            $http.post($rootScope.baseUrl + 'Account/ResetPassword?email='+vm.user.Email).then(function(res) {
                NProgress.done();
                console.log(res.data);
                if (res.data.success) {
                    vm.user = {};
                    toastr.success(res.data.message);
                    $location.path("/login");
                }
                else{
                    toastr.error(res.data.message);
                } 
            }, function(res) {
                console.dir(res);
                toastr.error('Reset Failed');
                NProgress.done();
            });
        };
    });
