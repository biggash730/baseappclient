'use strict';
/**
 * @ngdoc function
 * @name blumaApp.controller:QuicksendCtrl
 * @description
 * # QuicksendCtrl
 * Controller of the blumaApp
 */
angular.module('blumaApp').controller('QuicksendCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $('.menu .item').tab();
    $rootScope.page = "Send Messages";
    var vm = $scope;
    vm.message = {};
    vm.SMSTemplateSelected = function() {
        vm.message.SMessage = vm.message.STemplate.Message;
    };
    vm.EmailTemplateSelected = function() {
        vm.message.ESubject = vm.message.ETemplate.Title;
        vm.message.EMessage = vm.message.ETemplate.Message;
    };
    vm.getModels = function() {
        var obj = {
            Page: 0,
            Size: 0
        };
        $http.post($rootScope.baseUrl + 'settings/gettemplates', obj).then(function(res) {
            if (res.data.success) {
                vm.templates = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
        $http.post($rootScope.baseUrl + 'settings/getsenders', obj).then(function(res) {
            if (res.data.success) {
                vm.senderids = res.data.data;
            }
        }, function(res) {
            console.dir(res);
        });
    };
    vm.getModels()
    vm.sendSMSMessage = function() {
        //validate the form
        console.log(vm.message);
        NProgress.start();
        $http.post($rootScope.baseUrl + 'message/sendsmsmessage', vm.message).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                toastr.success(res.data.message);
                vm.message = {};
                $location.path("/messages");
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    };
    vm.sendEmailMessage = function() {
        //validate the form
        console.log(vm.message);
        NProgress.start();
        $http.post($rootScope.baseUrl + 'message/sendemailmessage', vm.message).then(function(res) {
            NProgress.done();
            //console.log(res.data);
            if (res.data.success) {
                toastr.success(res.data.message);
                vm.message = {};
                $location.path("/messages");
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            console.dir(res);
            toastr.error('Reset Failed');
            NProgress.done();
        });
    }
});