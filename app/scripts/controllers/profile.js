'use strict';
angular.module('blumaApp').controller('ProfileCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Profile";
    var vm = $scope;
    vm.user = vm.currentUser;
    console.log(vm.user);
    //vm.user.dateofbith = new Date(vm.user.dateofbith);
    vm.user.dateofbith = moment(vm.user.dateofbith,"YYYY-MM-DD").format('MM-DD-YYYY');
    vm.changePassword = function() {
        var err = '';
        if (!vm.user.OldPassword) err = err + 'Enter your current password. \n';
        if (!vm.user.NewPassword) err = err + 'Enter the new password. \n';
        if (!vm.user.ConfirmPassword) err = err + 'Repeat the new password. \n';
        if (vm.user.NewPassword != vm.user.ConfirmPassword) err = err + 'The new passwords do not match.'
        if (err != '') {
            toastr.error(err);
            return;
        } else {
            NProgress.start();
            $http.post($rootScope.baseUrl + 'Account/ChangePassword', vm.user).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success('Successful');
                    vm.user.OldPassword = "";
                    vm.user.NewPassword = "";
                    vm.user.ConfirmPassword = "";
                    $location.path("/");
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.dir(res);
                toastr.error('Failed');
                NProgress.done();
            });
        }
    };
    vm.update = function() {
        console.log(vm.user);
        vm.user.FullName = vm.user.fullname;
        vm.user.PhoneNumber = vm.user.phone;
        vm.user.Email = vm.user.email;
        
        vm.user.DateOfBirth = moment(vm.user.dateofbith,"DD/MM/YYYY").format('MM-DD-YYYY');

        var err = '';
        if (!vm.user.FullName) err = err + 'Please enter a full name \n';
        if (!vm.user.Email) err = err + 'Please enter your email \n';
        if (!vm.user.DateOfBirth) err = err + 'Please enter your date of birth \n';
        if (err != '') {
            toastr.error(err);
            return;
        } else {
            NProgress.start();
            $http.post($rootScope.baseUrl + 'Account/update', vm.user).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success(res.data.message);
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.dir(res);
                toastr.error('Failed');
                NProgress.done();
            });
        }
    };
});