'use strict';

describe('Controller: MakepurchaseCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var MakepurchaseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MakepurchaseCtrl = $controller('MakepurchaseCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MakepurchaseCtrl.awesomeThings.length).toBe(3);
  });
});
