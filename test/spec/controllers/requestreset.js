'use strict';

describe('Controller: RequestresetCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var RequestresetCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequestresetCtrl = $controller('RequestresetCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RequestresetCtrl.awesomeThings.length).toBe(3);
  });
});
