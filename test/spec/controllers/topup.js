'use strict';

describe('Controller: TopupCtrl', function () {

  // load the controller's module
  beforeEach(module('blumaApp'));

  var TopupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TopupCtrl = $controller('TopupCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TopupCtrl.awesomeThings.length).toBe(3);
  });
});
