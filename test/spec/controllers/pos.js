'use strict';

describe('Controller: PosCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var PosCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PosCtrl = $controller('PosCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PosCtrl.awesomeThings.length).toBe(3);
  });
});
