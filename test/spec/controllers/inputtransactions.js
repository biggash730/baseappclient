'use strict';

describe('Controller: InputtransactionsCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var InputtransactionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InputtransactionsCtrl = $controller('InputtransactionsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InputtransactionsCtrl.awesomeThings.length).toBe(3);
  });
});
