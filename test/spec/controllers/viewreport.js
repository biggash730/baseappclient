'use strict';

describe('Controller: ViewreportCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var ViewreportCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ViewreportCtrl = $controller('ViewreportCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ViewreportCtrl.awesomeThings.length).toBe(3);
  });
});
