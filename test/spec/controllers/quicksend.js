'use strict';

describe('Controller: QuicksendCtrl', function () {

  // load the controller's module
  beforeEach(module('blumaApp'));

  var QuicksendCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QuicksendCtrl = $controller('QuicksendCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(QuicksendCtrl.awesomeThings.length).toBe(3);
  });
});
