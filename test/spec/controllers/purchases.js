'use strict';

describe('Controller: PurchasesCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var PurchasesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PurchasesCtrl = $controller('PurchasesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PurchasesCtrl.awesomeThings.length).toBe(3);
  });
});
