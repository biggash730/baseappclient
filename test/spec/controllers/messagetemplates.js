'use strict';

describe('Controller: MessagetemplatesCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var MessagetemplatesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MessagetemplatesCtrl = $controller('MessagetemplatesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MessagetemplatesCtrl.awesomeThings.length).toBe(3);
  });
});
