'use strict';

describe('Controller: SendmessageCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var SendmessageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SendmessageCtrl = $controller('SendmessageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SendmessageCtrl.awesomeThings.length).toBe(3);
  });
});
