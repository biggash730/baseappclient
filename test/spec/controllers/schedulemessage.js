'use strict';

describe('Controller: SchedulemessageCtrl', function () {

  // load the controller's module
  beforeEach(module('blumaApp'));

  var SchedulemessageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchedulemessageCtrl = $controller('SchedulemessageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SchedulemessageCtrl.awesomeThings.length).toBe(3);
  });
});
