'use strict';

describe('Controller: SenderidsCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var SenderidsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SenderidsCtrl = $controller('SenderidsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SenderidsCtrl.awesomeThings.length).toBe(3);
  });
});
